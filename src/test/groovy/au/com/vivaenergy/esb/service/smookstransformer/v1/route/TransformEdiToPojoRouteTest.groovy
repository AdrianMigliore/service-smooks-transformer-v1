package au.com.vivaenergy.esb.service.smookstransformer.v1.route

import au.com.vivaenergy.esb.common.EsbUtils
import au.com.vivaenergy.esb.common.jaxrs.ObjectMapperFactory
import au.com.vivaenergy.esb.common.logging.AuditLogExchangeFormatter
import au.com.vivaenergy.esb.service.card.v1.models.Card
import au.com.vivaenergy.esb.service.card.v1.models.Identifier
import com.fasterxml.jackson.databind.ObjectMapper
import org.apache.camel.Processor
import org.apache.camel.builder.RouteBuilder
import org.apache.camel.component.log.LogComponent
import org.apache.camel.impl.JndiRegistry
import org.apache.camel.test.junit4.CamelTestSupport
import org.junit.Test
import org.milyn.edi.unedifact.d93a.BANSTA.Bansta
import org.milyn.edi.unedifact.d93a.D93AInterchangeFactory
import org.milyn.ejc.util.InterchangeTestUtil
import org.milyn.smooks.edi.unedifact.model.UNEdifactInterchange

import static au.com.vivaenergy.esb.common.cim.CimIdentifierUtils.createPublicList

class TransformEdiToPojoRouteTest extends CamelTestSupport{

	@Override
	protected RouteBuilder createRouteBuilder() throws Exception {
		return new TransformEdiToPojoRoute()
	}

	@Override
	protected JndiRegistry createRegistry() throws Exception {
		def registry =  super.createRegistry()

		registry.bind 'audit', new LogComponent(exchangeFormatter: new AuditLogExchangeFormatter())
		registry.bind 'esbUtils', new EsbUtils(appName: 'adapter-jde-work-order-test-v1')
		registry.bind 'factory', D93AInterchangeFactory.getInstance()

		return registry
	}

	@Override
	String isMockEndpointsAndSkip() {
		return ''
	}

	@Test
	void 'test success'() {
		def ex = template.request('direct:transformEdiToPojo', {
			it.in.body = getClass().getResourceAsStream('/in/DESADV.edi').text
		} as Processor)
	}

}
