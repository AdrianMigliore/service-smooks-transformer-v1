package au.com.vivaenergy.esb.service.smookstransformer.v1.helper

import org.milyn.edi.unedifact.d93a.D93AInterchangeFactory
import org.milyn.ejc.util.InterchangeTestUtil
import org.milyn.smooks.edi.unedifact.model.r41.UNEdifactInterchange41
import org.milyn.smooks.edi.unedifact.model.r41.UNEdifactMessage41

import java.nio.charset.StandardCharsets

class EdiHelper {

	static UNEdifactInterchange41 createInterchangeWithMessage(Class ediMessageClass) {
		return InterchangeTestUtil.buildInterchange([ediMessageClass] as Class[])
	}

	static String marshal(D93AInterchangeFactory factory, UNEdifactInterchange41 interchange) {
		StringWriter ediOutStream = new StringWriter()
		factory.toUNEdifact(interchange, ediOutStream)
		return ediOutStream.toString()
	}

	static List<UNEdifactMessage41> unmarshal(D93AInterchangeFactory factory, String ediInput) {
		factory.fromUNEdifact(new ByteArrayInputStream(ediInput.getBytes(StandardCharsets.UTF_8))).getMessages()
	}

}
