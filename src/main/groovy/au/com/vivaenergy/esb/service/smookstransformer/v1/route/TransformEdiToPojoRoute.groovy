package au.com.vivaenergy.esb.service.smookstransformer.v1.route

import au.com.vivaenergy.esb.common.jaxrs.ObjectMapperFactory
import au.com.vivaenergy.esb.service.card.v1.models.Card
import au.com.vivaenergy.esb.service.card.v1.models.Identifier
import org.apache.camel.Exchange
import org.apache.camel.builder.RouteBuilder
import org.apache.camel.component.jackson.JacksonDataFormat
import org.milyn.edi.unedifact.d93a.BANSTA.Bansta
import org.milyn.edi.unedifact.d93a.D93AInterchangeFactory
import org.milyn.ejc.util.InterchangeTestUtil
import org.milyn.smooks.edi.unedifact.model.UNEdifactInterchange
import org.milyn.smooks.edi.unedifact.model.r41.UNEdifactInterchange41

import static au.com.vivaenergy.esb.common.cim.CimIdentifierUtils.createPublicList

class TransformEdiToPojoRoute extends RouteBuilder {

	static final String OPERATION_NAME = 'transformEdiToPojo'
	static final String LOG_PREFIX = 'esb.smookstransformer-v1.transform'
	static final String EDI_HELPER = 'au.com.vivaenergy.esb.service.smookstransformer.v1.helper.EdiHelper'

	private JacksonDataFormat json = new JacksonDataFormat(ObjectMapperFactory.create(), Card)

	@Override
	void configure() throws Exception {
		configureMain()
	}

	void configureMain() {

		from("direct:${OPERATION_NAME}")
			.routeId("direct:${OPERATION_NAME}Route")

			.to("bean:esbUtils?method=pullMetaData('http')")
			.to("bean:esbUtils?method=initialiseContext('API', 'service-smooks-transform, OPERATION_NAME, 'API', 'Consumer')")

			//unmarshal and unpack the incoming EDI string to the List<messages> contained therein
			.beanRef(EDI_HELPER, "unmarshal(ref:factory," + '${body})')

			.setBody(simple('My DESADV ref number = ${body[0].message.segmentGroup1[0].reference.reference.referenceNumber}'))

			// Make sure no internals are leaked
			.removeHeaders('*', Exchange.HTTP_RESPONSE_CODE)

			.to("bean:esbUtils?method=pushMetaData('http')")
			.to("audit:${LOG_PREFIX}.output")
	}

}
