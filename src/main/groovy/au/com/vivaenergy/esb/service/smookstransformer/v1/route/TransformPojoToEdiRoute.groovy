package au.com.vivaenergy.esb.service.smookstransformer.v1.route

import au.com.vivaenergy.esb.common.jaxrs.ObjectMapperFactory
import au.com.vivaenergy.esb.service.card.v1.models.Card
import org.apache.camel.Exchange
import org.apache.camel.builder.RouteBuilder
import org.apache.camel.component.jackson.JacksonDataFormat
import org.milyn.edi.unedifact.d93a.DESADV.Desadv

class TransformPojoToEdiRoute extends RouteBuilder {

	static final String OPERATION_NAME = 'transformPojoToEdi'
	static final String LOG_PREFIX = 'esb.smookstransformer-v1.transform'
	static final String EDI_HELPER = 'au.com.vivaenergy.esb.service.smookstransformer.v1.helper.EdiHelper'

	private JacksonDataFormat json = new JacksonDataFormat(ObjectMapperFactory.create(), Card)

	Class desadv = Desadv.getClass()

	@Override
	void configure() throws Exception {
		from("direct:${OPERATION_NAME}")
			.routeId("direct:${OPERATION_NAME}Route")

			.to("bean:esbUtils?method=pullMetaData('http')")
			.to("bean:esbUtils?method=initialiseContext('API', 'service-card-v1, OPERATION_NAME, 'API', 'Consumer')")

			//NOTE: this is not used, its just to demonstrate how an inbound CIM string can be unmarshaled to CIM object for
			// further transformation later in route
			.unmarshal(json)

			//create a simple interchange with a test Message of type: "Desadv"
			//NOTE: For demonstration purposes only!!!
			.setBody(constant(Desadv))
			.beanRef(EDI_HELPER, "createInterchangeWithMessage(org.milyn.edi.unedifact.d93a.DESADV.Desadv)")

			//change message in some way. NOTE: in reality this would be a transformer to convert the input POJO -> EDI
			.process {
				it.in.body.getMessages()[0].message.segmentGroup1[0].reference.reference.referenceNumber = '<<<<<<<<< YO JOE >>>>>>>>>>'
			}

			//marshal the interchange to a String representation of the created EDI object
			.beanRef(EDI_HELPER, 'marshal(ref:factory,${body})')

			// Make sure no internals are leaked
			.removeHeaders('*', Exchange.HTTP_RESPONSE_CODE)

			.to("bean:esbUtils?method=pushMetaData('http')")
			.to("audit:${LOG_PREFIX}.output")
	}

}
